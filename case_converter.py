 
import json
from copy import deepcopy

# for beatiful output
import pprint


class LabelTransform(object):

    init_error_msg = 'Data should be: dict/list.'

    def __init__(self, transform_type):
        if transform_type == 'camel':
            self.transform = self._toCamelCase
        elif transform_type == 'snake':
            self.transform = self._to_snake_case
        else:
            raise Exception('No transformation defined for provided type.')
        
    def __call__(self, data, to_json=False):
        # make a deepcopy of data to not change the input dict
        data  = deepcopy(data)     
        data = self._validate_data(data)
        return self.change_labels(data, to_json)

    def _validate_data(self, data):
        if not any([isinstance(data, dict), isinstance(data, list)]):
            raise Exception(self.init_error_msg)        

        return data

    def change_labels(self, data, to_json):

        if isinstance(data, dict):
            data_copy = {}
            for key, value in data.iteritems():
                data_copy[self.transform(key)] = self.change_labels(value, to_json)
        elif isinstance(data, list):
            data_copy = []
            for elem in data:
                data_copy.append(self.change_labels(elem, to_json))
        else:
            return data

        return json.dumps(data_copy) if to_json else data_copy

    # according to pep8 this function should have different name :) but who cares ;) it's funny
    def _toCamelCase(self, label):
        if any([a.isupper() for a in label]):
            # maybe this is not needed: consider: 'label_A_something'... 
            # but otherwise: 'label_A_something' is not a camelCase
            return label
        sublabels = label.split('_')
        return ''.join([sublabels[0]] + [s.capitalize() for s in sublabels[1:]])


    def _to_snake_case(self, label):
        if '_' in label:
            return label
        new_label = map(lambda s: "_%s" % s.lower() if s.isupper() else s.lower(), label)
        return ''.join(new_label)

## TESTS

camel_case_data = {
    'firstName': 'Romuald', 
    'lastName': 'Majewski',
    'sex': 'male',
    'veryHighValue': 90,
    'comments': 
    [
        {
            'commentId': 12,
            'commentBody': 'It is very nice class :)',   
            'parentCommentId': None,
        },
    ],        
    'address': {
        'street': 'Grojecka 43',
        'zipCode': '02-008',
        'city': 'Warsaw',
        'userFlags': {
            'allowMarketing': True
        }
    }
}


snake_case_data = {
    'first_name': 'Marian',
    'last_name': 'Gorecki',
    'sex': 'male',
    'very_high_value': 90,
    'comments':
    [
        {
            'comment_id': 13,
            'comment_body': 'And I do not like it.',   
            'parent_comment_id': 12,
        }
    ],     
    'address': {
        'street': 'Mazowiecka 12',
        'zip_code': '02-024',
        'city': 'Warsaw',
        'user_flags': {
            'allow_marketing': True
        }
    }
}

snake_result = LabelTransform('snake')(camel_case_data)
camel_result = LabelTransform('camel')(snake_case_data)

# TEST OUTPUT
print(80*'-')
print(80*'-')
pp = pprint.PrettyPrinter(indent=4)
print(80*'-')
pp.pprint(camel_case_data)
print(80*'-')
pp.pprint(snake_result)
print(80*'-')
print(80*'-')

print(80*'-')
print(80*'-')
pp.pprint(snake_case_data)
print(80*'-')
pp.pprint(camel_result)
print(80*'-')
print(80*'-')

# SMALL TRICK

def snake_transform(data, to_json=False):
    return LabelTransform('snake')(data, to_json)

def camel_transform(data, to_json=False):
    return LabelTransform('camel')(data, to_json)


print('SNAKE')
data = snake_transform(camel_case_data)
print(data)
print(80*'--')
print('CAMEL')
data = camel_transform(snake_case_data)
print(data)

